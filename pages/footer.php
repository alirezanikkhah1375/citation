<footer>
    <div class="container footer">
        <div class="nav-footer">
            <a href="/index.php">HOME</a>
            <a href="/pages/authors.php">AUTHORS</a>
            <a href="./quotes.php">QUOTES</a>
        </div>
        <div class="logo">
            <img src="/assets/logo.png" alt="logo">
        </div>
        <div class="connection">
            <p>©copyright 2022 citation.fr</p>
        </div>
    </div>
</footer>