<?php
require './connection.php';

// Add new quote
if (!empty($_POST["quote-text"]) && !empty($_POST["author-select"])) {
    $quoteText = $_POST["quote-text"];
    $authorId = $_POST["author-select"];
    quoteAdder($pdo, $quoteText, $authorId);
}

// Delete a quote
if (!empty($_POST['quote-id-delete'])) {
    $quoteIdDelete = $_POST['quote-id-delete'];
    quoteRemover($pdo, $quoteIdDelete);
}

// Add new author
if (!empty($_POST["author-name"])) {
    $newAuthorName = $_POST["author-name"];
    authorAdder($pdo, $newAuthorName);
}

// Delete an author
if (!empty($_POST['author-id-delete'])) {
    $authorIdDelete = $_POST['author-id-delete'];
    authorRemover($pdo, $authorIdDelete);
}

/*   <------------------------------------------- functions ------------------------------------------->   */

function quoteAdder($connection, $quoteText, $authorId)
{
    try {
        $addQuote = $connection->prepare("INSERT INTO quotes(quote_content, author_id) VALUES (?,?)");
        $done = $addQuote->execute([$quoteText, $authorId]);
        if ($done) {
            header('Location: http://localhost:3000/pages/backOffice.php');
            die();
        }
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
}

function quoteRemover($connection, $quoteId)
{
    try {
        $addQuote = $connection->prepare("DELETE FROM quotes WHERE quote_id = ?");
        $done = $addQuote->execute([$quoteId]);
        if ($done) {
            header('Location: http://localhost:3000/pages/backOffice.php');
            die();
        }
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
}

function authorAdder($connection, $newAuthorName)
{
    try {
        $addQuote = $connection->prepare("INSERT INTO authors(author_name) VALUES (?)");
        $done = $addQuote->execute([$newAuthorName]);
        if ($done) {
            header('Location: http://localhost:3000/pages/backOffice.php');
            die();
        }
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
}

function authorModifier($connection, $newAuthorName, $authorId)
{
    try {
        $addQuote = $connection->prepare("UPDATE authors SET author_name = ? WHERE author_id = ?");
        $done = $addQuote->execute([$newAuthorName, $authorId]);
        if ($done) {
            header('Location: http://localhost:3000/pages/backOffice.php');
            die();
        }
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
}

function authorRemover($connection, $authorId)
{
    try {
        $addQuote = $connection->prepare("DELETE FROM authors WHERE author_id = ?");
        $done = $addQuote->execute([$authorId]);
        if ($done) {
            header('Location: http://localhost:3000/pages/backOffice.php');
            die();
        }
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
}
