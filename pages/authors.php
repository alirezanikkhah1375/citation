<?php
require './connection.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <title>citation</title>
</head>

<body>
    <?php include './header.php'; ?>
    <main>
        <div class="page-header">
            <img src="/assets/authors.jpg" alt="header-photo">
            <div class="page-title">
                <H1>
                    AUTHORS
                </H1>
            </div>
        </div>
        <div class="container">

            <?php
            $getAuthors = $pdo->prepare("SELECT authors.author_name, authors.author_id, quotes.author_id, quotes.quote_content FROM authors , quotes WHERE authors.author_id = quotes.author_id ORDER BY quote_id DESC");
            $getAuthors->execute();
            $authors = $getAuthors->fetchAll();

            foreach ($authors as $author) {
                echo '<br />' . $author['author_name'] . '<br/>';
                echo '<br />' . $author['quote_content'] . '<br/>';
            }

            ?>

        </div>
    </main>
    <?php include './footer.php'; ?>
</body>

</html>