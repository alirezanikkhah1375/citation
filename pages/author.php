<?php
require './connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <title>citation</title>
</head>

<body>
    <?php include './header.php'; ?>
    <main>
        <div class="page-header-author">
            <img src="/assets/pexels-arnie-chou-1151513.jpg" alt="header-photo">
            <div class="page-title">
                <H1>
                    AUTHORS
                </H1>
            </div>
        </div>
        <div class="container">

            <!-- i have to use id sent by url to search for content and id should be variable -->
            <?php
            $getAuthors = $pdo->prepare("select * from quotes, authors where authors.author_id = 4 AND authors.author_id = quotes.author_id ORDER BY quote_id DESC");
            $getAuthors->execute();
            $authors = $getAuthors->fetchAll();
            echo $authors[0]['author_name'] . '<br/>';

            foreach ($authors as $author) {
                echo $author['quote_content'] . '<br />';
            }

            ?>

        </div>
    </main>
    <?php include './footer.php'; ?>
</body>

</html>