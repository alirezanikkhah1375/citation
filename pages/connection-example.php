<?php

// database info
$host = 'localhost';
$db = 'citation';
$user = '';
$password = '';

$dsn = "mysql:host=$host;dbname=$db;charset=UTF8";

// PDO connection
try {
    $pdo = new PDO($dsn, $user, $password);

    // if ($pdo) {
    //     echo "Connected to the $db database successfully!";
    // }
} catch (PDOException $e) {
    echo $e->getMessage();
}

// display all errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
