<?php
    require './connection.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <title>citation</title>
</head>

<body>
    <?php include './header.php'; ?>
    <main>
        <div class="container">
            <section class="intro">
                <div class="intro-text">
                    <h3>“It is better to be hated for what you are than to be loved for what you are not.”</h3>
                    <p>― Andre Gide, Autumn Leaves</p>
                    <div class="flesh">
                        <img src="../assets/right-arrow.png" alt="arrow">
                    </div>
                </div>
                <div class="intro-photo">
                    <img src="../assets/intro.png" alt="image">
                </div>
            </section>

            <div class="divider div-transparent div-dot">
            </div>
            <section class="latest-quotes">
                <h2>LATEST QUOTES</h2>
                <div class="card">
                    <div class="left-quote"><img src="../assets/quote-icon.png" alt="quote"></div>
                    <div class="card-content">
                        <p>
                            “I'm selfish, impatient and a little insecure. I make mistakes, I am out of control and at times hard to handle. But if you can't handle me at my worst, then you sure as hell don't deserve me at my best.”
                        </p>
                        <p>
                            Marilyn Monroe
                        </p>
                    </div>
                    <div class="right-quote"><img src="../assets/quote-icon.png" alt="quote"></div>
                </div>
                <a href="#">VIEW MORE</a>
            </section>

        </div>
    </main>
    <?php include './footer.php'; ?>
</body>

</html>