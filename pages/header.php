<header>
    <div class="container header">
        <nav>
            <a href="/pages/index.php">HOME</a>
            <a href="/pages/authors.php">AUTHORS</a>
            <a href="/pages/quotes.php">QUOTES</a>
        </nav>
        <div class="logo">
            <img src="/assets/logo.png" alt="logo">
        </div>
        <div class="connection">
            <a href="#">CONNECTION</a>
            <span>|</span>
            <a href="#">INSCRIPTION</a>
        </div>
    </div>
</header>