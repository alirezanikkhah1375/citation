<?php
require './connection.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>

<body>


    <form class="text-add" action="/pages/queryManager.php" method="POST">
        <input type="text" name="quote-text" id="quote-text" placeholder="Quote text" required>
        <select name="author-select" id="author-select" required>
            <option hidden value=""></option>
            <?php

            $getAuthors = $pdo->prepare("SELECT * FROM authors ORDER BY author_name ASC");
            $getAuthors->execute();
            $authors = $getAuthors->fetchAll();

            foreach ($authors as $author) {
                echo '<option value=' . $author['author_id'] . '>' . $author['author_name'] . '</option>';
            }

            ?>
        </select>
        <button type="submit" name="add-quote-submit">Add New Quote</button>
    </form>


    <form class="author-add" action="/pages/queryManager.php" method="POST">
        <label for="author-name">Author Full Name</label>
        <input type="text" name="author-name" id="author-name" required>
        <button type="submit" name="add-quote-submit">Add New Author</button>
    </form>


    <div class="table-wrapper">
        <table class="fl-table">
            <thead>
                <tr>
                    <th>Author name</th>
                    <th>Modifications</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $getAuthors = $pdo->prepare("SELECT * FROM authors ORDER BY author_name ASC");
                $getAuthors->execute();
                $authors = $getAuthors->fetchAll();

                foreach ($authors as $author) {
                ?>

                    <tr>
                        <td><?= $author['author_name'] ?></td>
                        <td>
                            <form action="./queryManager.php" method="POST">
                                <input type="text" value="<?= $author['author_id'] ?>" name="author-id-delete" hidden>
                                <button type="submit">DELETE</button>
                            </form>
                        </td>
                    </tr>

                <?php } ?>
            <tbody>
        </table>
    </div>

    <div class="table-wrapper">
        <table class="fl-table">
            <thead>
                <tr>
                    <th>Author name</th>
                    <th>Quote</th>
                    <th>Modifications</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $getQuotes = $pdo->prepare("SELECT quote_id,authors.author_name, authors.author_id, quotes.author_id, quotes.quote_content FROM authors , quotes WHERE authors.author_id = quotes.author_id ORDER BY quote_id DESC");
                $getQuotes->execute();
                $quotes = $getQuotes->fetchAll();

                foreach ($quotes as $quote) {
                ?>

                    <tr>
                        <td><?= $quote['author_name'] ?></td>
                        <td><?= $quote['quote_content'] ?></td>
                        <td>
                            <form action="./queryManager.php" method="POST">
                                <input type="text" value="<?= $quote['quote_id'] ?>" name="quote-id-delete" hidden>
                                <button type="submit">DELETE</button>
                            </form>
                        </td>
                    </tr>

                <?php } ?>
            <tbody>
        </table>
    </div>
</body>

</html>